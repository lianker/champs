from distutils.core import setup


def read(filename):
    return [req.strip() for req in open(filename).readlines()]


setup(
    name="champs",
    version="0.1.0",
    py_modules=["champs"],
    author="lianker lopes",
    author_email="lianker.dev@gmail.com",
    url="https://gargula.space",
    description="my super dupper project champs",
    include_package_data=True,
    install_requires=read("requirements.txt"),
    extras_require={
        "development": read("requirements-dev.txt")
    },
    entry_points={'console_scripts': ['champs=champs:cli']}
)
