install:
	pip install -e .["development"]

uninstall:
	pip uninstall champs

test:
	pytest tests/ -v --cov=champs

clean:
	@find . -path ./.venv -prune -name '*.pyc' -exec rm -rf {} \;
	@find . -path ./.venv -prune -name '__pycache__' -exec rm -rf {} \;
	@find . -path ./.venv -prune -name 'Thumbs.db' -exec rm -rf {} \;
	@find . -path ./.venv -prune -name '*~' -exec rm -rf {} \;
	rm -rf .cache
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info
	rm -rf htmlcov
	rm -rf .tox/
	rm -rf .pytest_cache/
	rm -rf docs/_build
	pip install -e .[development] --upgrade --no-cache


format_code:
	autopep8 -r -i tests/ champs/
	isort --skip-gitignore champs/ tests/
