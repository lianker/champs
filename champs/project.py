import os
import shutil

import chevron

from champs.settings import Settings


class Project():
    def __init__(self, settings, name, pkg) -> None:
        self.settings: Settings = settings
        self.name = name
        self.pkg = pkg

    def clone_repo(self):
        clone_cmd = "git clone {0} {1}/{2}"\
            .format(self.settings.base_repo,
                    self.settings.project_location,
                    self.name)

        print("Cloning base repository")
        os.system(clone_cmd)

    def rename_pkg(self):
        base_path = "{0}/{1}/{2}"\
            .format(self.settings.project_location,
                    self.name,
                    self.settings.base_repo_name)

        pkg_path = "{0}/{1}/{2}"\
            .format(self.settings.project_location,
                    self.name,
                    self.pkg)

        print("Renaming project directories")
        os.system(f"mv {base_path} {pkg_path}")

    def get_template_values(self):
        return {
            "project_name": self.name,
            "project_module": self.pkg,
            "author": self.settings.author,
            "author_email": self.settings.author_email,
            "site": self.settings.site
        }

    def get_templates(self):
        return [
            {
                "template_name": "setup.mustache",
                "output_file": "setup.py"
            },
            {
                "template_name": "Makefile.mustache",
                "output_file": "Makefile"
            }
        ]

    def update_project_files(self, templates: dict,
                             values: dict,
                             proj_local: str,
                             proj_name: str):
        for template in templates:
            file_content = ""
            template_path = f"{proj_local}/{proj_name}/templates/{template['template_name']}"
            with open(template_path, 'r') as f:
                file_content = chevron.render(f.read(), values)

            output_file = open(
                f'{proj_local}/{proj_name}/{template["output_file"]}', "w")
            output_file.write(file_content)
            output_file.close()

        shutil.rmtree(f"{proj_local}/{proj_name}/templates/")

    def remove_old_git(self):
        try:
            git_dir = "{0}/{1}/.git"\
                .format(self.settings.project_location,
                        self.name)

            print(f"Removing: {git_dir}")
            shutil.rmtree(git_dir)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))

    def create(self):
        self.clone_repo()
        self.rename_pkg()

        values = self.get_template_values()

        mustache_templates = self.get_templates()

        self.update_project_files(mustache_templates,
                                  values,
                                  self.settings.project_location,
                                  self.name)

        self.remove_old_git()
