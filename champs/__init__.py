import click

from champs.project import Project
from champs.settings import Settings


@click.group()
def cli():
    """root command"""
    pass


@cli.command()
@click.option("--pkg-name", help="name of main package")
@click.argument('name')
def create_pkg(pkg_name, name):
    pkg = pkg_name or name
    click.echo(f'creating package: {name}, main_pkg: {pkg}')

    stg = Settings("/home/lianker/dvlopment/.champsrc")
    Project(stg, name, pkg).create()
