import os


class Settings():
    def __init__(self, env_path) -> None:
        self.load(env_path)

        self.base_repo = os.environ.get("BASE_REPO")
        self.base_repo_name = os.environ.get("BASE_REPO_NAME")

        self.project_location = os.environ.get("PROJECT_LOCATION")
        self.site = os.environ.get("SITE")
        self.author = os.environ.get("AUTHOR")
        self.author_email = os.environ.get("AUTHOR_EMAIL")

    def load(self, env_path):
        with open(env_path, "r") as settings:
            env_vars = settings.read()
            for cmd in env_vars.split("\n"):
                if cmd != "":
                    key, value = cmd.split("=")
                    os.environ[key] = value


if __name__ == "__main__":
    rc_path = "/home/lianker/dvlopment/.champsrc"
    s = Settings(rc_path)

    print(s.author_email)
    print(s.author)
